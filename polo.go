package main

import (
	"fmt"
	"github.com/jyap808/go-poloniex"
)

const (
	API_KEY    = ""
	API_SECRET = ""
)

type Polo struct {
	symbols  []string
	tickers  map[string]poloniex.Ticker
	poloniex *poloniex.Poloniex
}

func NewPolo(symbols []string) *Polo {
	poloniex := poloniex.New(API_KEY, API_SECRET)
	polo := &Polo{symbols: symbols, poloniex: poloniex}
	return polo
}

func (polo *Polo) GetPrices() ([][]string, error) {
	tickers, err := polo.poloniex.GetTickers()
	if err != nil {
		return nil, err
	}
	polo.tickers = tickers
	filteredTickers := polo.filter()
	prices := [][]string{}
	for k, v := range filteredTickers {
		item := []string{k, v.Last.String()}
		prices = append(prices, item)
	}
	return prices, nil
}

func (polo *Polo) filter() map[string]poloniex.Ticker {
	res := map[string]poloniex.Ticker{}
	for k, v := range polo.tickers {
		if StringInArray(k, polo.symbols) > -1 {
			res[k] = v
		}
	}
	return res
}

func StringInArray(key string, array []string) int {
	for index, value := range array {
		if value == key {
			return index
		}
	}
	return -1
}

func main() {
	symbols := []string{"USDT_REP", "USDT_BCH", "BTC_ETC", "USDT_ETH", "USDT_LTC", "USDT_BTC", "BTC_ETH", "USDT_ETC"}
	polo := NewPolo(symbols)
	prices, err := polo.GetPrices()
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+#v", prices)
}
