package main

import (
	"fmt"
	"github.com/bitfinexcom/bitfinex-api-go/v1"
)

func main() {
	client := bitfinex.NewClient().Auth("api-key", "api-secret")
	ticker, err := client.Ticker.Get("BTCUSD")
	fmt.Printf("%+v", ticker)
	stat, err := client.Stats.All("BTCUSD", "", "")
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v", stat)
}
