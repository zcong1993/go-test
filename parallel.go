package main

import (
	"fmt"
	"sync"
	"time"
)

func delay(in string) string {
	time.Sleep(time.Second * 1)
	return in
}

func main() {
	arr := []string{"zc", "l", "q"}
	wg := sync.WaitGroup{}
	res := make([]string, len(arr))
	for i, v := range arr {
		wg.Add(1)
		i, v := i, v
		go func() {
			res[i] = delay(v)
			defer wg.Done()
		}()
	}
	wg.Wait()
	fmt.Printf("%+v\n", res)
}
