package mock_test

import (
	assert2 "github.com/stretchr/testify/assert"
	"gitlab.com/zcong1993/go-test/mock"
	"gitlab.com/zcong1993/go-test/mock/mocks"
	"testing"
)

func TestGetPersonMsg(t *testing.T) {
	assert := assert2.New(t)
	p := &mock.Person{"zcong", 18}
	api := new(mocks.Api)
	api.On("GetPerson").Return(p)
	name, age := mock.GetPersonMsg(api)
	assert.Equal(name, p.Name, "name")
	assert.Equal(age, p.Age, "age")
}
