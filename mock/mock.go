package mock

type Api interface {
	GetName() string
	GetPerson() *Person
}

type Person struct {
	Name string
	Age  int
}

func GetPersonMsg(api Api) (string, int) {
	p := api.GetPerson()
	return p.Name, p.Age
}
