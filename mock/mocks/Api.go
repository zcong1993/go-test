// Code generated by mockery v1.0.0
package mocks

import go_testmock "gitlab.com/zcong1993/go-test/mock"
import mock "github.com/stretchr/testify/mock"

// Api is an autogenerated mock type for the Api type
type Api struct {
	mock.Mock
}

// GetName provides a mock function with given fields:
func (_m *Api) GetName() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// GetPerson provides a mock function with given fields:
func (_m *Api) GetPerson() *go_testmock.Person {
	ret := _m.Called()

	var r0 *go_testmock.Person
	if rf, ok := ret.Get(0).(func() *go_testmock.Person); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*go_testmock.Person)
		}
	}

	return r0
}
