package main

import (
	"fmt"
	"github.com/gorilla/websocket"
	"time"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second
)

type Ws struct {
	url          string
	conn         *websocket.Conn
	cmds         []interface{}
	done         chan *Ws
	handler      func(msg []byte)
	pingInterval time.Duration
}

func NewWs(url string, cmds []interface{}) *Ws {
	return &Ws{
		url:  url,
		cmds: cmds,
		done: make(chan *Ws),
	}
}

func (ws *Ws) SetHandler(handler func(msg []byte)) {
	ws.handler = handler
}

func (ws *Ws) connect() error {
	c, _, err := websocket.DefaultDialer.Dial(ws.url, nil)
	if err != nil {
		println("conn err")
		return err
	}
	ws.conn = c
	return nil
}

func (ws *Ws) ping() {
	if ws.pingInterval == time.Duration(0) {
		ws.pingInterval = time.Second * 1
	}
	ticker := time.NewTicker(ws.pingInterval)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			println("ping")
			ws.conn.SetWriteDeadline(time.Now().Add(writeWait))
			err := ws.conn.WriteMessage(websocket.PingMessage, nil)
			if err != nil {
				ws.done <- ws
				return
			}
		}
	}
}

func (ws *Ws) Run() {
	if ws.handler == nil {
		ws.handler = func(msg []byte) {
			fmt.Printf("%s \n", msg)
		}
	}
	defer func() {
		println("err")
		ws.done <- ws
	}()
	err := ws.connect()
	if err != nil {
		return
	}
	for _, v := range ws.cmds {
		err := ws.conn.WriteJSON(v)
		if err != nil {
			return
		}
	}
	for {
		_, msg, err := ws.conn.ReadMessage()
		if err != nil {
			return
		}
		ws.handler(msg)
	}
}

func (ws *Ws) Close() {
	if ws.conn == nil {
		return
	}
	err := ws.conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	if err != nil {
		println(err)
		return
	}
	time.Sleep(time.Second)
	println("close")
	ws.conn.Close()
}

func main() {
	//ws := NewWs("wss://stream.binance.com:9443/ws/bnbbtc@ticker", []interface{}{})
	ws := NewWs("ws://localhost:8080", []interface{}{})
	go ws.Run()
	for {
		select {
		case w := <-ws.done:
			ws.Close()
			println("rec")
			time.Sleep(time.Second)
			go w.Run()
			ws = w
		}
	}
}
