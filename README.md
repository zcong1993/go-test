# go-test
<!--
[![Go Report Card](https://goreportcard.com/badge/github.com/zcong1993/go-test)](https://goreportcard.com/report/github.com/zcong1993/go-test)
-->

> my go project

```go
package main

import (
    "github.com/microcosm-cc/bluemonday"
    "gopkg.in/russross/blackfriday.v2"
    "fmt"
    "io/ioutil"
)

func main() {
    input, _ := ioutil.ReadFile("./README.md")
    unsafe := blackfriday.Run([]byte(input))
    html := bluemonday.UGCPolicy().SanitizeBytes(unsafe)
    fmt.Printf("%s", html)
}
```

## License

MIT &copy; zcong1993
