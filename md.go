package main

import (
	"fmt"
	"github.com/microcosm-cc/bluemonday"
	"gopkg.in/russross/blackfriday.v2"
	"io/ioutil"
	"regexp"
)

func main() {
	input, _ := ioutil.ReadFile("./README.md")
	unsafe := blackfriday.Run([]byte(input))
	p := bluemonday.UGCPolicy()
	p.AllowAttrs("class").Matching(regexp.MustCompile("^language-[a-zA-Z0-9]+$")).OnElements("code")
	html := p.SanitizeBytes(unsafe)
	fmt.Printf("%s", html)
}
