package main

import (
	"fmt"
	"time"
)

type Result struct {
	Index   int
	Input   string
	Res     string
	Timeout bool
}

func mockApi(out string, ch chan<- string) {
	time.Sleep(time.Second * 2)
	ch <- out
}

//func conc(input []string) map[int]Result {
//	res := map[int]Result{}
//	ch := make(chan string, len(input))
//	for index, ip := range input {
//		go mockApi(ip, ch)
//		res[index] = Result{
//			Index:index,
//			Input:ip,
//		}
//	}
//	for i:=0;i<len(input);i++ {
//		select {
//		case <-time.After(time.Second * 1):
//
//		}
//	}
//}

func main() {
	ch := make(chan string, 1)
	go mockApi("test", ch)
	select {
	case <-time.After(time.Second * 1):
		fmt.Println("timeout")
	case result := <-ch:
		fmt.Printf(result)
	}
}
