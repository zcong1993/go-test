package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

func prt(v interface{}) {
	fmt.Printf("%+#v\n", v)
}

func main() {
	person := &Person{Name: "zcong", Age: 18}
	p := *person
	p.Name = "l"
	prt(p)
	prt(*&person)
}
