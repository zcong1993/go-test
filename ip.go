package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/", func(c *gin.Context) {
		fmt.Printf("%s\n", c.GetHeader("X-Forwarded-For"))
		c.JSON(200, gin.H{
			"name": "zcong",
			"age":  18,
		})
	})
	r.Run()
}
