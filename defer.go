package main

func def(i int) int {
	println("in")
	if i < 10 {
		return i
	}
	defer func() {
		println("defer")
	}()
	return i
}

func main() {
	def(1)
	def(11)
}
