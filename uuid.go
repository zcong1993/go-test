package main

import (
	"fmt"
	"github.com/oklog/ulid"
	"math/rand"
	"time"
)

func newId() string {
	entropy := rand.New(rand.NewSource(time.Now().UnixNano()))
	uuid := ulid.MustNew(ulid.Now(), entropy).String()
	return uuid
}

func main() {
	for i := 0; i < 100; i++ {
		fmt.Println(newId())
	}
}
