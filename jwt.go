package main

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/kataras/iris/core/errors"
	"github.com/prometheus/common/log"
	"time"
)

const key = "zcong"

var method = jwt.SigningMethodHS256

func Encode(cm *jwt.MapClaims) (string, error) {
	token := jwt.NewWithClaims(method, cm)
	tokenString, err := token.SignedString([]byte(key))
	return tokenString, err
}

func Decode(t string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(t, func(tk *jwt.Token) (interface{}, error) {
		return []byte(key), nil
	})
	if err != nil {
		return nil, err
	}
	if !token.Valid {
		return nil, errors.New("token is not valid")
	}
	return token.Claims.(jwt.MapClaims), nil
}

func main() {
	cm := &jwt.MapClaims{
		"name": "zcong",
		"age":  18,
		"exp":  time.Now().Add(time.Second * 60).Unix(),
	}
	tk, err := Encode(cm)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(tk)
	t := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZ2UiOjE4LCJleHAiOjE1MTI2MjYxMjUsIm5hbWUiOiJ6Y29uZyJ9.rItJgZE-MliREo2TLZFItTj57L2ZGMNvmIoFrZ7Ug20"
	c, err := Decode(t)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%+#v", c)
}
