package main

import (
	"fmt"
	"time"
)

func createCh() chan time.Time {
	ch := make(chan time.Time, 4)
	ticker := time.NewTicker(time.Second)
	go func() {
		for {
			select {
			case <-ticker.C:
				ch <- time.Now()
			}
		}
	}()
	return ch
}

func main() {
	ch := createCh()
	for {
		select {
		case t := <-ch:
			fmt.Println(t)
		}
	}
}
