package main

import (
	"context"
	"fmt"
)

// Ctx is wrapper of context
type Ctx struct {
	context.Context
}

// CtxMap is map type for Ctx.SetMap
type CtxMap map[string]interface{}

// NewCtx create a new Ctx
func NewCtx(ctx context.Context) *Ctx {
	return &Ctx{
		ctx,
	}
}

// Set add k, v to context
func (c *Ctx) Set(k, v interface{}) {
	c.Context = context.WithValue(c.Context, k, v)
}

// SetMap range and set k, v to context
func (c *Ctx) SetMap(mp *CtxMap) {
	for k, v := range *mp {
		c.Set(k, v)
	}
}

// Get can get context value by key
func (c *Ctx) Get(k interface{}) interface{} {
	return c.Context.Value(k)
}

func main() {
	base := context.TODO()
	ctx := NewCtx(base)
	ctx.Set("zcong", "lys")
	mp := &CtxMap{
		"name": "zcong",
		"age":  18,
	}
	ctx.SetMap(mp)
	fmt.Println(ctx.Get("zcong"), ctx.Get("name"), ctx.Get("age"))
}
