package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		for _, v := range r.Header {
			fmt.Printf("%+#v\n", v)
		}
		fmt.Printf("%s\n", r.Header.Get("X-Forwarded-For"))
		w.Write([]byte("test"))
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
