package main

import "fmt"

type R interface {
	Echo(interface{}) interface{}
	Eat(string)
}

type Cat struct {
	Name string
}

func (c *Cat) Echo(in interface{}) interface{} {
	return in
}

func (c *Cat) Eat(food string) {
	fmt.Printf("%s eat food : %s\n", c.Name, food)
}

type Man struct {
	Name string
	Age  int
}

func (c *Man) Echo(in interface{}) interface{} {
	return c
}

func (c *Man) Eat(food string) {
	fmt.Printf("%s eat food : %s\n", c.Name, food)
}

func Eat(p R, food string) {
	p.Eat(food)
}

func main() {
	cat := &Cat{"catcat"}
	man := &Man{"zcong", 18}
	food := "fish"
	Eat(cat, food)
	Eat(man, food)
}
